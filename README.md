# react-native-posts-zemoga

> This project is a test for Zemoga. I'm going to develop an app to list posts from fake Online REST API

## Requirements

- [NodeJS v13.8.0](https://nodejs.org/en/);
- [Yarn v1.22.0](https://yarnpkg.com/en/);
- [React v16.11.0](https://reactjs.org/);
- [React Native v0.62.2](https://facebook.github.io/react-native/docs/getting-started);

## How to run locally

- Navigate to the project folder: `cd ./react_native_posts_zemoga`;
- Install project dependencies: `yarn`;
- Install CocoaPods iOS packages: `yarn pod-install`;
- Run in iOS Simulator: `yarn ios`;
- Run in Android Simulator: `yarn android`;

## Structure folder

components :

- atoms: `The component most small(Text,Card, Icons )`
- molecules `Composition of several component`

navigation:

This folder contain the navigation of all aplication

resources :

- providers: `data origin(Post, users,comments, realm(post))`, i put all data origin here

- repositories `repositories for each type of entity , I organized all data processing from de providers`

screens:

Screen of the application

- Post
- PostDetail

## CONCLUSION

I decided to use Realm to manage the large data got from `https://jsonplaceholder.typicode.com/`

I didn't use Redux or React Context, it was a bad decision, i thought that it was not necessary but finally when I was deleting a post and adding to post in favorites, it was a bit difficult manage of that manner.

I wanted update the posts lists in background each time that i read or added a post, i did it but the performance was bad, i removed this feature, to see it the post update you must refresh the posts manually
