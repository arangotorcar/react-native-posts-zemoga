import { URL_BASE } from '../constants'
const URL_POSTS = 'posts'

const getPosts = async () => {
  const URL_REQUEST = `${URL_BASE}${URL_POSTS}`
  try {
    const responseJson = await fetch(URL_REQUEST)
    const response = await responseJson.json()
    return response
  } catch (error) {
    console.log(error)
    return []
  }
}

export { getPosts }
