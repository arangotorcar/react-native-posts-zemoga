import { URL_BASE } from '../constants'
const URL_USERS = 'users'

const getUserById = async (id) => {
  const URL_REQUEST = `${URL_BASE}${URL_USERS}/${id}`
  try {
    const responseJson = await fetch(URL_REQUEST)
    const response = await responseJson.json()
    return response
  } catch (error) {
    console.log(error)
    return []
  }
}

export { getUserById }
