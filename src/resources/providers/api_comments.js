import { URL_BASE } from '../constants'
const URL_COMMENTS = 'comments'

const getComments = () => {}

const getCommentsByPostId = async (postId) => {
  const URL_REQUEST = `${URL_BASE}${URL_COMMENTS}?postId=${postId}`
  console.log(URL_REQUEST)

  try {
    const responseJson = await fetch(URL_REQUEST)
    const response = await responseJson.json()
    return response
  } catch (error) {
    console.log(error)
    return []
  }
}

export { getComments, getCommentsByPostId }
