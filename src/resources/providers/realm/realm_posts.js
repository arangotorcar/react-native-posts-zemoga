import realmConnection from '.'
import { POST_SCHEMA_CONSTANT } from './schemas'

const create = async (post) => {
  const realm = await realmConnection()
  realm.write(() => {
    realm.create(POST_SCHEMA_CONSTANT, post)
  })
}

const get = async () => {
  const realm = await realmConnection()
  let posts = realm.objects(POST_SCHEMA_CONSTANT)
  return posts
}

const getFavorites = async () => {
  const realm = await realmConnection()
  let posts = realm.objects(POST_SCHEMA_CONSTANT).filtered('isFavorite = true')
  return posts
}

const getFavoriteByPostId = async (id) => {
  const realm = await realmConnection()
  let posts = realm
    .objects(POST_SCHEMA_CONSTANT)
    .filtered(`id="${id}" AND  isFavorite = true`)
  return posts
}

const update = async (id, data) => {
  const realm = await realmConnection()
  realm.write(() => {
    realm.create(POST_SCHEMA_CONSTANT, { id, ...data }, 'modified')
  })
  realm.close()
}

const destroy = async (id) => {
  const realm = await realmConnection()
  realm.write(() => {
    let posts = realm.objects(POST_SCHEMA_CONSTANT).filtered(`id = "${id}"`)
    realm.delete(posts)
  })
}

const destroyAll = async () => {
  const realm = await realmConnection()
  realm.write(() => {
    let post = realm.objects(POST_SCHEMA_CONSTANT)
    realm.delete(post)
  })
}

export {
  create,
  update,
  get,
  destroy,
  destroyAll,
  getFavorites,
  getFavoriteByPostId
}
