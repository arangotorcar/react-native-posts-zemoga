import { CommentSchema, PostSchema, UserSchema } from './schemas'

const Realm = require('realm')

const realm = async () => {
  return await Realm.open({
    schema: [CommentSchema, PostSchema, UserSchema]
  })
}

export default realm
