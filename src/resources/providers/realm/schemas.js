const POST_SCHEMA_CONSTANT = 'post'
const COMMENT_SCHEMA_CONSTANT = 'comment'
const USER_SCHEMA_CONSTANT = 'user'
const FAVORITE_POST_SCHEMA_CONSTANT = 'favorite_post'

const PostSchema = {
  name: POST_SCHEMA_CONSTANT,
  primaryKey: 'id',
  properties: {
    id: 'int',
    userId: 'int',
    title: 'string',
    body: 'string',
    isRead: { type: 'bool', optional: true },
    isFavorite: { type: 'bool', optional: true }
  }
}

const CommentSchema = {
  primaryKey: 'id',
  name: COMMENT_SCHEMA_CONSTANT,
  properties: {
    id: 'int',
    name: 'string',
    email: 'string',
    body: 'string',
    post: 'post'
  }
}

const UserSchema = {
  name: USER_SCHEMA_CONSTANT,
  properties: {
    id: 'int',
    name: 'string',
    username: 'string',
    email: 'string',
    website: 'string'
  }
}

export {
  POST_SCHEMA_CONSTANT,
  COMMENT_SCHEMA_CONSTANT,
  USER_SCHEMA_CONSTANT,
  FAVORITE_POST_SCHEMA_CONSTANT,
  PostSchema,
  CommentSchema,
  UserSchema
}
