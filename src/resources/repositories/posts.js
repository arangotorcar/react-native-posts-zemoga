import { getPosts as getPostsProvider } from '../providers/api_posts'
import { getCommentsByPostId as getCommentsByPostIdProvider } from '../providers/api_comments'
import {
  get as getRealmPostProvider,
  create as createRealmPostProvider,
  update as updateRealmPostProvider,
  destroy as destroyRealmPostProvider,
  destroyAll as destroyAllRealmPostProvider,
  getFavorites as getFavoritesRealmPostProvider,
  getFavoriteByPostId as getFavoriteByPostIdRealmPostProvider
} from '../providers/realm/realm_posts'

const getPosts = async () => {
  let postsLocal = await getRealmPostProvider()

  if (postsLocal.length == 0) {
    const posts = await getPostsProvider()
    for (let i = 0; i < posts.length; i++) {
      const post = posts[i]
      await createRealmPostProvider(post)
    }
    return posts
  }

  return postsLocal
}

const getFavoritePosts = async () => {
  return await getFavoritesRealmPostProvider()
}

const updateReadPost = async (id) => {
  return await updateRealmPostProvider(id, { isRead: true })
}

const manageFavoritePost = async (id) => {
  const posts = await getFavoriteByPostIdRealmPostProvider(id)
  if (posts.length > 0) {
    await updateRealmPostProvider(id, { isFavorite: false })
    return 'deleted'
  }
  await updateRealmPostProvider(id, { isFavorite: true })
  return 'added'
}

const getCommentsByPostId = async (postId) => {
  return await getCommentsByPostIdProvider(postId)
}

const destroyPost = async (id) => {
  return await destroyRealmPostProvider(id)
}

const destroyPosts = async () => {
  return await destroyAllRealmPostProvider()
}

export {
  getPosts,
  getCommentsByPostId,
  updateReadPost,
  destroyPost,
  destroyPosts,
  getFavoritePosts,
  manageFavoritePost
}
