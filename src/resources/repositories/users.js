import { getUserById as getUserByIdProvider } from '../providers/api_users'

const getUserById = async (id) => {
  return await getUserByIdProvider(id)
}

export { getUserById }
