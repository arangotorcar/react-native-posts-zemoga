import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Posts, PostDetail } from '../screens'
const Stack = createStackNavigator()

function HomeStackNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#10c64e'
        },
        headerTintColor: '#fff'
      }}>
      <Stack.Screen name={'Posts'} component={Posts} />
      <Stack.Screen name={'PostDetail'} component={PostDetail} />
    </Stack.Navigator>
  )
}

export default HomeStackNavigator
