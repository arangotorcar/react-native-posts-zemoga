import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import HomeStackNavigator from './home-navigator'

const Stack = createStackNavigator()

export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name={'Home'} component={HomeStackNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
