import React from 'react'
import { StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'
import { Icons } from '../../atoms'

const ButtonDelete = ({ onPress }) => {
  return (
    <SafeAreaView>
      <TouchableOpacity onPress={() => onPress()} style={styles.button}>
        <Icons.Trash />
      </TouchableOpacity>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius: 25,
    bottom: 10,
    elevation: 9,
    height: 50,
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4
    },

    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    width: 50
  }
})
export default ButtonDelete
