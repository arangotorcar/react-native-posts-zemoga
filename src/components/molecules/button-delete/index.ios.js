import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

const ButtonDelete = ({ onPress }) => {
  return (
    <TouchableOpacity onPress={() => onPress()} style={styles.button}>
      <Text style={styles.text}>DELETE ALL</Text>
    </TouchableOpacity>
  )
}

export default ButtonDelete

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: 'red',
    height: 50,
    justifyContent: 'center'
  },
  text: { color: 'white' }
})
