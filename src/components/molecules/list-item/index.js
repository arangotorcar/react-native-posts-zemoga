import React from 'react'
import { StyleSheet, View, TouchableOpacity, Platform } from 'react-native'
import { Card, Text, Icons } from '../../atoms'

import Swipeable from 'react-native-gesture-handler/Swipeable'

const PostItem = ({
  iconLeft,
  isVisibleIconLeft,
  iconRight,
  isVisibleIconRight = true,
  body = '',
  onPress,
  disabled,
  onSwipeRightDelete
}) => {
  const RightActions = ({ onRightPress }) => {
    return (
      <TouchableOpacity onPress={onRightPress} style={styles.rightAction}>
        <Icons.Trash />
        <Text style={{}}>Delete</Text>
      </TouchableOpacity>
    )
  }
  return (
    <Swipeable
      enabled={!disabled}
      renderRightActions={() => (
        <RightActions onRightPress={onSwipeRightDelete} />
      )}>
      <TouchableOpacity disabled={disabled} onPress={onPress}>
        <Card>
          <View
            style={[
              styles.viewWrapIconLeft,
              Platform.OS == 'android' && !iconLeft
                ? styles.viewWrapIconLeftNotVisible
                : {}
            ]}>
            {isVisibleIconLeft && iconLeft}
          </View>
          <View style={styles.viewWrapBody}>
            <Text style={styles.textBody}>{body}</Text>
          </View>
          {isVisibleIconRight && iconRight}
        </Card>
        <View style={styles.viewBorderBottom} />
      </TouchableOpacity>
    </Swipeable>
  )
}

export default PostItem

const styles = StyleSheet.create({
  viewBorderBottom: {
    backgroundColor: 'white',
    borderBottomWidth: 0.5,
    marginLeft: Platform.OS == 'ios' ? 20 : 0
  },
  viewWrapIconLeft: { width: 15 },
  viewWrapIconLeftNotVisible: { width: 0 },
  viewWrapBody: { flex: 1, paddingLeft: Platform.OS == 'ios' ? 15 : 10 },
  textBody: { marginRight: 10 },
  rightAction: {
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  }
})
