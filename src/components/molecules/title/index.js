import React from 'react'
import { StyleSheet } from 'react-native'
import { Text } from '../../atoms'

const Title = (props) => {
  return <Text style={styles.title}>{props.children}</Text>
}

export default Title

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  }
})
