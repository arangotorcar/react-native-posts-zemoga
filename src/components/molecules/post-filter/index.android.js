/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
  Easing
} from 'react-native'

const { width } = Dimensions.get('screen')
const PostsFilters = ({ onPress, tabs = [] }) => {
  const activeTabIndicator = new Animated.Value(0)
  const [activeTab, setActiveTab] = useState('ALL')
  React.useEffect(() => {}, [])

  const changeActiveTab = (index) => {
    Animated.timing(activeTabIndicator, {
      toValue: parseInt((width / tabs.length) * index),
      duration: 400,
      useNativeDriver: false,
      easing: Easing.back(1)
    }).start(() => {
      setActiveTab(tabs[index].id)
    })
  }

  return (
    <View style={styles.viewWrap}>
      {tabs.map((item, index, tabs) => {
        return (
          <View style={styles.viewWrapItem} key={item.id}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                changeActiveTab(index)
                onPress(item)
              }}
              style={[styles.button]}>
              <Text
                style={{
                  color: activeTab == item.id ? 'white' : 'rgba(255,255,255,.5)'
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          </View>
        )
      })}
      <Animated.View
        style={{
          height: 4,
          width: [100 / tabs.length, '%'].join(''),
          backgroundColor: 'white',
          position: 'absolute',
          left: activeTabIndicator,
          bottom: 0
        }}
      />
    </View>
  )
}

export default PostsFilters

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#10c64e',
    height: 45,
    justifyContent: 'center'
  },
  viewWrap: {
    elevation: 7,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65
  },
  viewWrapItem: { flex: 1, width }
})
