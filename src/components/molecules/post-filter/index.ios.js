/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const PostsFilters = ({ onPress, tabs }) => {
  const [activeTab, setActiveTab] = useState('ALL')

  return (
    <View style={styles.viewWrap}>
      {tabs.map((item, index, tabs) => {
        let borderStyle = {}
        if (index == 0) {
          borderStyle = styles.buttonLeft
        }
        if (index == tabs.length - 1) {
          borderStyle = styles.buttonRight
        }
        return (
          <TouchableOpacity
            key={item.id}
            onPress={() => {
              onPress(item)
              setActiveTab(item.id)
            }}
            style={[
              styles.button,
              borderStyle,
              {
                backgroundColor: activeTab == item.id ? '#10c64e' : 'white'
              }
            ]}>
            <Text style={{ color: activeTab == item.id ? 'white' : '#10c64e' }}>
              {item.name}
            </Text>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

export default PostsFilters

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  buttonLeft: {
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5
  },
  buttonRight: {
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5
  },
  viewWrap: {
    borderColor: '#10c64e',
    borderRadius: 5,
    borderWidth: 1,
    flexDirection: 'row',
    height: 30,
    marginHorizontal: 25,
    marginTop: 10
  }
})
