import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const FeaturedSection = ({ sectionTitle }) => {
  return (
    <View style={styles.viewComment}>
      <Text>{sectionTitle}</Text>
    </View>
  )
}

export default FeaturedSection

const styles = StyleSheet.create({
  viewComment: {
    backgroundColor: '#DBD9DA',
    padding: 5,
    paddingLeft: 15
  }
})
