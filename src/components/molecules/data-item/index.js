import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Text } from '../../atoms'

const DataItem = ({ field = '', value = '' }) => {
  return (
    <View style={styles.viewItem}>
      <Text>{field}:</Text>
      <Text> {value}</Text>
    </View>
  )
}

export default DataItem

const styles = StyleSheet.create({
  viewItem: { flexDirection: 'row', flexWrap: 'wrap', marginVertical: 5 }
})
