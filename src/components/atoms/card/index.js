import React from 'react'
import { StyleSheet, View } from 'react-native'

const Card = (props) => {
  return <View style={styles.card}>{props.children}</View>
}

export default Card

const styles = StyleSheet.create({
  card: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10
  }
})
