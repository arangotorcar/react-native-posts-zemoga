import React from 'react'
import { StyleSheet, Text as TextRN, Platform } from 'react-native'

const Text = ({ children, style }) => {
  return (
    <TextRN
      adjustsFontSizeToFit={Platform.OS == 'android'}
      allowFontScaling={false}
      style={[styles.text, style]}>
      {children}
    </TextRN>
  )
}

export default Text

const styles = StyleSheet.create({
  text: {
    fontSize: 14
  }
})
