import React from 'react'
import { StyleSheet, SafeAreaView } from 'react-native'

const Container = (props) => {
  return <SafeAreaView style={styles.container}>{props.children}</SafeAreaView>
}

export default Container

const styles = StyleSheet.create({
  container: { flex: 1 }
})
