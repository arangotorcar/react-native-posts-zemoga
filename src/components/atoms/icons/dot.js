import React from 'react'
import { StyleSheet, View } from 'react-native'

const Dot = () => {
  return <View style={styles.dot} />
}

const styles = StyleSheet.create({
  dot: {
    backgroundColor: 'blue',
    borderRadius: 7.5,
    height: 15,
    width: 15
  }
})

export default Dot
