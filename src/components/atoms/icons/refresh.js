import React from 'react'
import { StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Refresh = ({ onPress }) => (
  <Icon
    onPress={() => onPress()}
    style={styles.refresh}
    name='ios-refresh'
    size={25}
    color='white'
  />
)

export default Refresh

const styles = StyleSheet.create({
  refresh: {
    height: 24,
    width: 24
  }
})
