import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons'

const ArrowRight = () => {
  return <Icon name='ios-arrow-forward' size={20} color='grey' />
}

export default ArrowRight
