import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons'

const Trash = () => {
  return <Icon name='delete' size={20} color='white' />
}

export default Trash
