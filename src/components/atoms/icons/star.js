import React from 'react'
import { StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Star = ({ isActive, onPress }) => (
  <Icon
    onPress={() => onPress()}
    style={styles.refresh}
    name={isActive ? 'ios-star' : 'ios-star-outline'}
    size={25}
    color={isActive ? 'yellow' : 'white'}
  />
)

export default Star

const styles = StyleSheet.create({
  refresh: {
    height: 24,
    width: 24
  }
})
