import Dot from './dot'
import ArrowRight from './arrow-right'
import Refresh from './refresh'
import Trash from './trash'
import Star from './star'
export { Dot, ArrowRight, Refresh, Trash, Star }
