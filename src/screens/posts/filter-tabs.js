const TAB_1 = 'ALL'
const TAB_2 = 'FAVORITES'

const tabs = [
  {
    id: TAB_1,
    name: 'All'
  },
  {
    id: TAB_2,
    name: 'Favorites'
  }
]
export { tabs, TAB_1, TAB_2 }
