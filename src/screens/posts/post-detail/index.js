/* eslint-disable react/display-name */
import React, { useEffect, useState } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import {
  Title,
  DataItem,
  FeaturedSection,
  ListItem
} from '../../../components/molecules'
import { Container, Text, Icons } from '../../../components/atoms'
import { getUserById } from '../../../resources/repositories/users'
import {
  getCommentsByPostId,
  manageFavoritePost
} from '../../../resources/repositories/posts'

const DESCRIPTION_TITLE = 'Description'
const USER_TITLE = 'User'
const COMMENT_SECTION_TITLE = 'COMMENTS'

const PostDetail = ({ route, navigation }) => {
  const { post } = route.params
  const [user, setUser] = useState({})
  const [comments, setComments] = useState([])

  const userId = post.userId
  const postId = post.id

  const fetchUser = async () => {
    try {
      const user = await getUserById(userId)
      setUser(user)
    } catch (error) {
      console.log(error)
      alert(error.message)
    }
  }

  const fetchCommentsByPostId = async () => {
    try {
      const comments = await getCommentsByPostId(postId)
      setComments(comments)
    } catch (error) {
      console.log(error)
      alert(error.message)
    }
  }

  const HeaderRight = () => {
    const [isFavorite, setIsFavorite] = useState(post.isFavorite)
    return (
      <View style={styles.viewWrapRightIconHeader}>
        <Icons.Star
          isActive={isFavorite}
          onPress={async () => {
            const status = await manageFavoritePost(postId)
            setIsFavorite(status == 'added')
          }}
        />
      </View>
    )
  }

  useEffect(() => {
    fetchUser()
    fetchCommentsByPostId()
    navigation.setOptions({
      headerRight: () => <HeaderRight />
    })
  }, [])

  const renderCommentItem = ({ item }) => {
    return <ListItem disabled={true} body={item.body} />
  }

  return (
    <Container>
      <FlatList
        ListHeaderComponent={
          <>
            <View style={styles.viewSection}>
              <Title>{DESCRIPTION_TITLE}</Title>
              <View style={styles.viewBody}>
                <Text style={styles.textBody}>{post.body}</Text>
              </View>
            </View>
            <View style={styles.viewSection}>
              <Title>{USER_TITLE}</Title>
              <View style={styles.viewUserData}>
                <DataItem field={'Name'} value={user.name} />
                <DataItem field={'Email'} value={user.email} />

                <DataItem field={'Phone'} value={user.phone} />
                <DataItem field={'Website'} value={user.website} />
              </View>
            </View>
            <View style={[styles.viewSection, styles.viewSectionNotPH]}>
              <FeaturedSection sectionTitle={COMMENT_SECTION_TITLE} />
            </View>
          </>
        }
        showsVerticalScrollIndicator={false}
        data={comments}
        contentContainerStyle={{ paddingBottom: 40 }}
        initialNumToRender={10}
        removeClippedSubviews
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderCommentItem}
      />
    </Container>
  )
}

export default PostDetail

const styles = StyleSheet.create({
  viewBody: {
    marginVertical: 10
  },
  viewSection: {
    marginHorizontal: 15,
    marginTop: 10
  },
  viewSectionNotPH: { marginHorizontal: 0 },
  viewUserData: {
    marginVertical: 10
  },
  textBody: { lineHeight: 20 },
  viewWrapRightIconHeader: { marginRight: 10 }
})
