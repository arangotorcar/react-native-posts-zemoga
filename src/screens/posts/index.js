import React, { useEffect, useState } from 'react'
import { FlatList, View, StyleSheet, Platform } from 'react-native'

// Repositories
import {
  getPosts,
  updateReadPost,
  destroyPosts,
  getFavoritePosts,
  destroyPost
} from '../../resources/repositories/posts'
// Custom components
import { Icons, Container } from '../../components/atoms'
import {
  ListItem,
  ButtonDelete,
  PostsFilters
} from '../../components/molecules'

import { tabs, TAB_1 } from './filter-tabs'

const Posts = ({ navigation }) => {
  const [posts, setPosts] = useState([])
  const [filterTabSelected, setFilterTabSelected] = useState(TAB_1)
  const [isLoading, setIsLoading] = useState(false)

  const fetchPosts = async () => {
    try {
      setIsLoading(true)
      const posts = await getPosts()
      setPosts(posts)
      setIsLoading(false)
    } catch (error) {
      setPosts([])
      alert('Ha ocurrido un error')
      console.log('ERROR-fetchPosts', error)
    }
  }

  const fetchFavoritePosts = async () => {
    try {
      setIsLoading(true)
      const posts = await getFavoritePosts()
      setPosts(posts)
      setIsLoading(false)
    } catch (error) {
      setPosts([])
      alert('Ha ocurrido un error')
      console.log('ERROR-fetchFavoritePosts', error)
    }
  }

  const manageFetchPosts = (filterTabSelected) => {
    filterTabSelected == TAB_1 ? fetchPosts() : fetchFavoritePosts()
  }

  const HeaderRight = (filterTabSelected) => {
    return (
      <View style={styles.viewWrapRightIconHeader}>
        <Icons.Refresh
          onPress={() => {
            manageFetchPosts(filterTabSelected)
          }}
        />
      </View>
    )
  }

  useEffect(() => {
    fetchPosts()
    navigation.setOptions({
      headerRight: () => HeaderRight(filterTabSelected)
    })
  }, [navigation])

  const onPressFilter = (item) => {
    setFilterTabSelected(item.id)
    manageFetchPosts(item.id)
  }

  const onPressDeleteAll = () => {
    destroyPosts()
    setPosts([])
  }

  const onPressPostItem = async (item, navigation) => {
    try {
      await updateReadPost(item.id)
      navigation.navigate('PostDetail', {
        post: {
          ...item
        }
      })
    } catch (error) {
      alert('Ha ocurrido un error')
      console.log('ERROR-onPressPostItem', error)
    }
  }

  const onDelete = async ({ item }) => {
    await destroyPost(item.id)
    await fetchPosts()
  }

  const renderItem = ({ item, index }, navigation, onDelete) => {
    let iconLeft = null
    let isVisibleIconLeft = true
    if (!item.isRead && !item.isFavorite) {
      iconLeft = index < 20 ? <Icons.Dot /> : null
    }
    if (item.isRead && item.isFavorite) {
      iconLeft = Platform.OS == 'ios' ? <Icons.Star isActive /> : null
    }

    if (index > 20) {
      isVisibleIconLeft = item.isFavorite
    }

    return (
      <ListItem
        onSwipeRightDelete={onDelete}
        onPress={() => onPressPostItem(item, navigation)}
        isVisibleIconLeft={isVisibleIconLeft}
        iconLeft={iconLeft}
        iconRight={
          Platform.OS == 'ios' ? (
            <Icons.ArrowRight />
          ) : item.isFavorite ? (
            <Icons.Star isActive />
          ) : null
        }
        body={item.body}
      />
    )
  }

  return (
    <Container>
      <PostsFilters tabs={tabs} onPress={onPressFilter} />
      <View style={styles.viewPosts}>
        <FlatList
          contentContainerStyle={styles.flContainerPost}
          refreshing={isLoading}
          onRefresh={() => manageFetchPosts(filterTabSelected)}
          data={posts}
          removeClippedSubviews
          extraData={posts}
          initialNumToRender={10}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(object) =>
            renderItem(object, navigation, () => onDelete(object))
          }
        />
      </View>
      <ButtonDelete onPress={onPressDeleteAll} />
    </Container>
  )
}

const styles = StyleSheet.create({
  viewPosts: {
    marginTop: 10,
    flex: 1
  },
  flContainerPost: { paddingBottom: 60, paddingTop: 20 },
  viewWrapRightIconHeader: { marginRight: 10 }
})
export default Posts
