module.exports = {
  trailingComma: 'none',
  tabWidth: 2,
  semi: false,
  jsxSingleQuote: true,
  requireConfig: true,
  singleQuote: true,
  insertPragma: false,
  proseWrap: 'never',
  bracketSpacing: true,
  jsxBracketSameLine: true,
  arrowParens: 'always'
}
